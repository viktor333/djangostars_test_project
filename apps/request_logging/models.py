from django.db import models


class RequestRequest(models.Model):
    response = models.SmallIntegerField()
    method = models.CharField(max_length=7)
    path = models.CharField(max_length=255)
    time = models.DateTimeField()
    is_secure = models.BooleanField()
    is_ajax = models.BooleanField()
    ip = models.GenericIPAddressField()
    referer = models.CharField(max_length=255, blank=True, null=True)
    user_agent = models.CharField(max_length=255, blank=True, null=True)
    language = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'request_request'
