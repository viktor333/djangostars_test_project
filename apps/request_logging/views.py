from django.views.generic import ListView

from request_logging.models import RequestRequest


class RequestsListView(ListView):
    model = RequestRequest
    template_name = 'requests_logging/requests_list.html'
    paginate_by = 10
    ordering = '-time'
