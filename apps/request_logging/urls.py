from django.urls import path

from . import views

app_name = 'request_logging'

urlpatterns = [
    path('', views.RequestsListView.as_view(), name='list'),
]
