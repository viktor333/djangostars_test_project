from django import forms

from books.models import Books


class BooksForm(forms.ModelForm):
    required_css_class = 'error'

    class Meta:
        model = Books
        fields = ['title', 'authors_info', 'isbn', 'publish_date', 'price']
