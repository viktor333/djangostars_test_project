from django.core.management import BaseCommand

from books.models import Books


class Command(BaseCommand):

    def handle(self, *args, **options):
        if options['ascending'] or (not options['ascending'] and not options['descending']):
            books = Books.objects.order_by('publish_date')
        elif options['descending']:
            books = Books.objects.order_by('-publish_date')
        elif options['ascending'] and options['descending']:
            raise Exception('You can\'t use ascending and descending ordering at the same time')
        for book in books:
            print(book.title, str(book.publish_date))

    def add_arguments(self, parser):
        parser.add_argument(
            '-asc',
            '--ascending',
            action='store_true',
            default=False,
            help='Ordering by by publish_date ascending'
        )
        parser.add_argument(
            '-desc',
            '--descending',
            action='store_true',
            default=False,
            help='Ordering by publish_date descending'
        )
