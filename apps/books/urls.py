from django.urls import path

from . import views

app_name = 'books'

urlpatterns = [
    path('', views.BooksListView.as_view(), name='list'),
    path('logging/', views.BooksLoggingView.as_view(), name='logging'),
    path('create/', views.CreateBookView.as_view(), name='create'),
    path('<pk>/', views.UpdateBookView.as_view(), name='update'),
    path('<pk>/delete/', views.DeleteBookView.as_view(), name='delete')
]
