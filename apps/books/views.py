from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from books.forms import BooksForm
from books.models import Books, BooksLogging


class BooksListView(ListView):
    model = Books
    template_name = 'books/books_list.html'
    ordering = '-publish_date'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ordering'] = self.get_ordering()
        return context

    def get_ordering(self):
        if 'ordering' in self.request.GET.keys():
            self.ordering = 'publish_date' if self.request.GET['ordering'].startswith('-') else '-publish_date'
        return self.ordering


class CreateBookView(CreateView):
    model = Books
    form_class = BooksForm
    template_name = 'books/create_book.html'
    success_url = reverse_lazy('books:list')


class UpdateBookView(UpdateView):
    model = Books
    form_class = BooksForm
    template_name = 'books/update_book.html'
    success_url = reverse_lazy('books:list')


class DeleteBookView(DeleteView):
    model = Books
    success_url = reverse_lazy('books:list')


class BooksLoggingView(ListView):
    model = BooksLogging
    template_name = 'books/books_logging_list.html'
