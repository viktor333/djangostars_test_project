from django.utils import timezone
from django.db import models
from django.db.models import DateField


class Books(models.Model):
    title = models.CharField(max_length=80)
    authors_info = models.CharField(max_length=100)
    isbn = models.CharField(max_length=20)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    publish_date = DateField(default=timezone.now)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        manipulation_type = 'update' if self.pk else 'create'
        BooksLogging.objects.create(manipulation_type=manipulation_type, book_title=self.title)
        super(Books, self).save(force_insert, force_update, using, update_fields)

    def delete(self, using=None, keep_parents=False):
        BooksLogging.objects.create(manipulation_type='delete', book_title=self.title)
        super(Books, self).delete(using, keep_parents)

    class Meta:
        db_table = 'books'


class BooksLogging(models.Model):
    manipulation_type = models.CharField(max_length=10)
    book_title = models.CharField(max_length=80, null=True, blank=True)
    time = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'books_logging'
