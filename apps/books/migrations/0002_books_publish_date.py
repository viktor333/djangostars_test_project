# Generated by Django 2.1.2 on 2018-10-27 15:50

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='books',
            name='publish_date',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
